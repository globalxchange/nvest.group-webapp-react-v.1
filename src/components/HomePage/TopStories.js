import React,{useEffect,useState} from 'react'
import Carousel from "react-multi-carousel";
import './HomePage.scss'
import "react-multi-carousel/lib/styles.css";
import {Homes} from './HomeApi'
import p1 from '../../static/images/p1.png'
import p2 from '../../static/images/p2.png'
import p3 from '../../static/images/p3.png'
import p4 from '../../static/images/p4.png'
import s1 from '../../static/images/s1.png'
import s2 from '../../static/images/s2.png'
import s3 from '../../static/images/s3.png'
import { HashLink } from 'react-router-hash-link';
import nation from '../../static/images/nation.png'
import axios from 'axios'
import arrow from '../../static/images/arrow.png'

import nv from '../../static/images/nv.png'
import so1 from '../../static/images/so1.png'
import so2 from '../../static/images/so2.png'
import so3 from '../../static/images/so3.png'


const slatform=[
  {
    img:s1,
  },
  {
    img:s2,
  },
  {
    img:s3,
  },
]
const platform=[
  {
    img:p1,
  },
  {
    img:p2,
  },
  {
    img:p3,
  },
  {
    img:p4,
  },
]
const stories=[
  {
    img:nation,
    text:"Nations Group Is An Automated Investment Syndicate Which Invests Into Brands That Develop Domestic Marketplaces With A Given Country.",
    button:"3 Brands"
  },
  {
    img:nation,
    text:"Nations Group Is An Automated Investment Syndicate Which Invests Into Brands That Develop Domestic Marketplaces With A Given Country.",
    button:"3 Brands"
  },
  {
    img:nation,
    text:"Nations Group Is An Automated Investment Syndicate Which Invests Into Brands That Develop Domestic Marketplaces With A Given Country.",
    button:"3 Brands"
  },
  {
    img:nation,
    text:"Nations Group Is An Automated Investment Syndicate Which Invests Into Brands That Develop Domestic Marketplaces With A Given Country.",
    button:"3 Brands"
  }
]
const responsive = {
  desktop: {
    breakpoint: { max: 10000, min: 1024 },
    items: 9,
    partialVisibilityGutter: 40,
    slidesToSlide: 9
  },
  tablet: {
    breakpoint: { max: 1024, min: 464 },
    items: 2,

    slidesToSlide: 2
  },
  mobile: {
    breakpoint: { max: 464, min: 0 },
    items: 2,

    slidesToSlide: 2,
  }
};
const images = [

  "https://images.unsplash.com/photo-1550133730-695473e544be?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=800&q=60",
  "https://images.unsplash.com/photo-1550167164-1b67c2be3973?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=800&q=60",
  "https://images.unsplash.com/photo-1550338861-b7cfeaf8ffd8?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=800&q=60",
  
  "https://images.unsplash.com/photo-1550353175-a3611868086b?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=800&q=60",
  "https://images.unsplash.com/photo-1550330039-a54e15ed9d33?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=800&q=60",
  "https://images.unsplash.com/photo-1549737328-8b9f3252b927?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=800&q=60",
  "https://images.unsplash.com/photo-1549833284-6a7df91c1f65?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=800&q=60",
  "https://images.unsplash.com/photo-1549985908-597a09ef0a7c?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=800&q=60",
  "https://images.unsplash.com/photo-1550064824-8f993041ffd3?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=800&q=60"
];

export default function MainCarsole() {
   const [prev,setprev]=useState(0)
   const [next,setnext]=useState(9)
const [elemetitem,setelemetitem]=useState([])
const [belemetitem,setbelemetitem]=useState([])
   const nextFucntion=(e)=>{
    if(next<18){
    setprev(next)
    setnext(next + e)
    }   
   }

   const prevFucntion=(e)=>{
if(prev>0){
    setprev(prev - e)
    setnext(next -e)
}

   }

   const fethbrandimgae=async()=>{

    let data = await axios.get(
      "https://comms.globalxchange.io/gxb/app/gxlive/operator/brands/get"
    );
    if (data.data.status) {
      console.log("data.data.apps", data.data.apps);
    setbelemetitem(data.data.brands)
      // setbackgroudimage(data.data.apps[0].cover_photo);
      // setappicon(data.data.apps[0].app_icon);
    }
  
  }

 
  const fethimgae=async()=>{

    let data = await axios.get(
      "https://comms.globalxchange.io/gxb/app/gxlive/operator/app/groups/get"
    );
    if (data.data.status) {
      console.log("data.data.apps", data.data.apps);
    setelemetitem(data.data.groups)
      // setbackgroudimage(data.data.apps[0].cover_photo);
      // setappicon(data.data.apps[0].app_icon);
    }
  
  }


    useEffect(() => {

      fethimgae()
      fethbrandimgae()
        let multiples=[];
        let finial=[]
        for (var i = 0; i <= Homes.length; i++) {
            if (i % 9 === 0) { // divide by the number
              multiples.push(i); // add the current multiple found to the multiples array
            }
            else{
               
            }
          }
          for (var i = 1; i < multiples.length; i++) {
            if (i % 1 === 0) { // divide by the number
                finial.push(i); // add the current multiple found to the multiples array
            }
            else{
               
            }
          }
      
        
        return () => {
        }
    }, [])

    console.log("prev", prev)
    console.log("next", next)
    return (
        <>
        {/* <HashLink to="#tops">
        <div className="SotriesMain">
<div class="block-heading" >
Indices

</div>
    

    <div className="storiesclass">
    {elemetitem.map((item)=>{
      console.log("sadasdasd",item.other_data[0])
      return(
        <div className="substory">
          <img src={item.logo} alt="" />
          <p>{item.description}</p>
          <label htmlFor="" onClick={()=> window.open(item.website, '_blank')} style={{background:`#${item.other_data.colorcode}`}}>Website</label>
        </div>
      )
    })
    }
    </div>
 

 <div className="seeall">
   <h3>See All</h3>
   <img src={arrow} alt="" />
 </div>
        </div>
        </HashLink>

        <div className="SotriesMain">
<div class="block-heading">
Companies

</div>
    

<div className="storiesclassbrand">
    {belemetitem.slice(0,7).map((item)=>{
      console.log("sadasdasd",item.other_data[0])
      return(
<div className="plp">


        <div className="substory">
          <img src={item.other_data.coloredfulllogo} alt="" />
          <p>{item.description}</p>
          
          <label htmlFor="" onClick={()=> window.open(item.website, '_blank')} style={{background:`#${item.other_data.primarycolourcode}`}}>Website</label>
       
      
        </div>
        <div className="outsidebrand">
        <img src={item.parent_groupData.icon} alt="" />
        </div>
        </div>
      )
    })
    }
    </div>
 

 <div className="seeall">
   <h3>See All</h3>
   <img src={arrow} alt="" />
 </div>
 
        </div>



        <div className="SotriesMain">
<div class="block-heading">
Platforms

</div>
    

    <div className="storiesclassplatorm">
    {platform.map((item)=>{
  
      return(
        <div className="substory1">
          <img src={item.img} alt="" />
         
        </div>
      )
    })
    }
    </div>
 


        </div>



        <div className="SotriesMain">
<div class="block-heading">
Protocols

</div>
    

    <div className="storiesclassprotocal">
    {slatform.map((item)=>{
  
      return(
        <div className="substory2">
          <img src={item.img} alt="" />
         
        </div>
      )
    })
    }
    </div>
 


        </div>


        <div className="footerend">
<div>
  <img src={nv} alt="" />
</div>

<div className="subk">
<img src={so1} alt="" />
<img src={so2} alt="" />
  <img src={so3} alt="" />
</div>
        </div> */}

</>

    )
}
