import React,{useEffect} from 'react'
import Carousel from "react-multi-carousel";
import './PopularCatories.scss'
import "react-multi-carousel/lib/styles.css";
import {Homes} from './HomeApi'
import cashback from '../../static/images/cashback.png'
import TopStories from './TopStories'


const list=[
    {
        img:"https://d3pzq99hz695o4.cloudfront.net/sitespecific/in/coupon-categories/mweb/5f735b1ef3026be39a6d1c66e7fe4a19/food-and-dining-icon-grey-small.png?193371",
name:"FOOD & DINING"
},
{
    img:"https://d3pzq99hz695o4.cloudfront.net/sitespecific/in/coupon-categories/mweb/cce80a72bc3481d723c38cccf592d45a/fashion-icon-grey-small.png?210460",
    name:"FASHION"
    },
    {
       img: "https://d3pzq99hz695o4.cloudfront.net/sitespecific/in/coupon-categories/mweb/2fc8b475c69d8e6248bf5a3aef2c9388/beauty-and-health-icon-grey-small.png?335877",
        name:"BEAUTY & HEALTH"
        },
        {
           img: "https://d3pzq99hz695o4.cloudfront.net/sitespecific/in/coupon-categories/mweb/f03d845e0abf31e72409cf7c5c704a2e/travel-icon-grey-small.png?110365",
            name:"TRAVEL"
            },
]
const responsive1 = {
  desktop: {
    breakpoint: { max: 10000, min: 1024 },
    items: 6,
    partialVisibilityGutter: 40,
    slidesToSlide: 6
  },
  tablet: {
    breakpoint: { max: 1024, min: 464 },
    items: 2,

    slidesToSlide: 2
  },
  mobile: {
    breakpoint: { max: 464, min: 0 },
    items: 2,

    slidesToSlide: 2,
  }
};

const responsive = {
    desktop: {
      breakpoint: { max: 10000, min: 1024 },
      items: 4,
      partialVisibilityGutter: 40,
      slidesToSlide: 4
    },
    tablet: {
      breakpoint: { max: 1024, min: 464 },
      items: 2,
  
      slidesToSlide: 2
    },
    mobile: {
      breakpoint: { max: 464, min: 0 },
      items: 2,
  
      slidesToSlide: 2,
    }
  };
const images = [
"https://d3pzq99hz695o4.cloudfront.net/sitespecific/in/banner/web/686b05560a692e65d0176eda6e7cd6c9/banner_home-290x580.jpg?335174",
  "https://d3pzq99hz695o4.cloudfront.net/sitespecific/in/banner/web/be77deea2adf00e2beec14a316bed6dc/banner_home-290x580.jpg?526085",
  "https://d3pzq99hz695o4.cloudfront.net/sitespecific/in/banner/web/066e4334c4f3e7879a6606040b498282/banner_home-290x580.jpg?609092",
  "https://d3pzq99hz695o4.cloudfront.net/sitespecific/in/banner/web/3d8fcab9d683b8c86f3cb3fa3ab89563/banner_home-290x580.jpg?672518",
  "https://d3pzq99hz695o4.cloudfront.net/sitespecific/in/banner/web/c96091c368bcb58249891929c6222e24/banner_home-290x580.jpg?467538",
  "https://images.unsplash.com/photo-1550353175-a3611868086b?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=800&q=60",

 "https://d3pzq99hz695o4.cloudfront.net/sitespecific/in/banner/web/d368acbff0cfca8c7521d9a8575d6229/banner_home-290x580.jpg?331310",
  "https://images.unsplash.com/photo-1550330039-a54e15ed9d33?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=800&q=60",
  "https://images.unsplash.com/photo-1549737328-8b9f3252b927?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=800&q=60",
  "https://images.unsplash.com/photo-1549833284-6a7df91c1f65?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=800&q=60",
  "https://images.unsplash.com/photo-1549985908-597a09ef0a7c?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=800&q=60",
  "https://images.unsplash.com/photo-1550064824-8f993041ffd3?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=800&q=60"
];

export default function MainCarsole() {
  const [name,setname]=React.useState("FOOD & DINING") 

    return (
        <>
               <div class="block-heading-cutom" >
               POPULAR CATEGORIES

</div>
<div className="selector">
    {
        list.map(item=>{
            return(
<div className="card"style={name==item.name?{background:"#0000ff0a"}:{}} onClick={()=>setname(item.name)}>
    <img src={item.img} alt=""/>
    <p>{item.name}</p>
</div>
            )
        })
    }
</div>
        <div className="conbinsection">

 
        <div className="carsoulesectioncashback " style={{height: "13rem"}}>
        <div class="block-heading" style={{marginTop:"0",paddingTop:"2.5rem"}}>
        TRENDING STORES

</div>
                <Carousel
                swipeable={false}
                draggable={false}
  ssr
  infinite={false}
  autoPlay={false}
  autoPlaySpeed={1000}
  keyBoardControl={false}
  customTransition="all .8"
  transitionDuration={500}
  containerClass="carousel-container"
  responsive={responsive1}
  minimumTouchDrag={80}
  deviceType="desktop"
  dotListClass="custom-dot-list-style"

    >
      {Homes.map(item => {
        return (
            <div className="cashbakcard">
            <img src={item.img} alt=""/> 
          
        <h2>Upto {item.offer} CD Rewards</h2>

        </div>
      
        );
      })}
    </Carousel>


        </div>

        <div className="carsoulesectioncashback ">
        <div class="block-heading">
        TRENDING OFFERS

</div>
                <Carousel
                swipeable={false}
                draggable={false}
  ssr
  infinite={false}
  autoPlay={false}
  autoPlaySpeed={1000}
  keyBoardControl={false}
  customTransition="all .8"
  transitionDuration={500}
  containerClass="carousel-container"
  responsive={responsive}
  minimumTouchDrag={80}
  deviceType="desktop"
  dotListClass="custom-dot-list-style"

    >
      {Homes.map(item => {
        return (
            <div className="cashbakcard">
            <img src={item.img} alt=""/> 
            <h5>{item.name}</h5>
        <h2>Upto {item.offer} CD Rewards</h2>
        <label >Activite Rewards</label>
        </div>
      
        );
      })}
    </Carousel>

    </div>
        </div>
</>
    )
}
