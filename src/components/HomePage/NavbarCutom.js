
import React,{useEffect,useState,useRef,useContext} from 'react'
import search from '../../static/images/search.png'
import M  from './MainCarsole'
import couple from '../../static/images/couple.png'
import sing from '../../static/images/sing.png'
import industires from '../../static/images/industires.png'
import brand from '../../static/images/brand.png'
import bcomp from '../../static/images/bcomp.png'
import venusb from '../../static/images/venusb.png'
import scrolllogo from '../../static/images/nvestlogo.png'
import logo from '../../static/images/fulllogo.png'
import './NabvarHome.scss'
import mlogo from '../../static/images/mlogo.png'
import searchh from '../../static/images/searchh.png'
import venu from '../../static/images/venu.png'
import comp from '../../static/images/comp.png'
import setting from '../../static/images/setting.png'
import Skelton from './Skelton'
import inventry from '../../static/images/inventry.png'
import { useHistory, useParams } from "react-router-dom";
import { HashLink  } from 'react-router-hash-link';
import {Link} from 'react-scroll'
import Carousel from "react-multi-carousel";

import './HomePage.scss'
import "react-multi-carousel/lib/styles.css";
import {Homes} from './HomeApi'
import { Agency } from '../../context/Context';
import burger from '../../static/images/burger.png'
import val from '../../static/images/val.png'
import p1 from '../../static/images/p1.png'
import p2 from '../../static/images/p2.png'
import p3 from '../../static/images/p3.png'
import p4 from '../../static/images/p4.png'
import s1 from '../../static/images/s1.png'
import s2 from '../../static/images/s2.png'
import s3 from '../../static/images/s3.png'

import nation from '../../static/images/nation.png'
import axios from 'axios'
import arrow from '../../static/images/arrow.png'

import nv from '../../static/images/nv.png'
import so1 from '../../static/images/so1.png'
import so2 from '../../static/images/so2.png'
import so3 from '../../static/images/so3.png'



const scrollToRef = (ref) => window.scrollTo(0, 1000) 
const slatform=[
  {
    img:sing,
    url:"https://vault.storage/",
  },
  {
    img:s1,
    url:"https://sharetokens.com/",

  },
  {
    img:s2,
    url:"https://bond.markets/",
 
  },
  {
    img:s3,
    url:"https://indexfunds.co/",
  },
]
const platform=[
  {
    img:val,
    url:"https://businesses.app/",
  },
  {
    img:p1,
    url:"https://startupbrokers.com/",

  },
  {
    img:p2,
    url:"https://venture.markets/",
  },
  {
    img:p3,
    url:"https://tokenapps.com/",
   
  },
  {
    img:p4,
    url:"https://virtualprospectus.com/",
  },
]
const stories=[
  {
    img:nation,
    text:"Nations Group Is An Automated Investment Syndicate Which Invests Into Brands That Develop Domestic Marketplaces With A Given Country.",
    button:"3 Brands"
  },
  {
    img:nation,
    text:"Nations Group Is An Automated Investment Syndicate Which Invests Into Brands That Develop Domestic Marketplaces With A Given Country.",
    button:"3 Brands"
  },
  {
    img:nation,
    text:"Nations Group Is An Automated Investment Syndicate Which Invests Into Brands That Develop Domestic Marketplaces With A Given Country.",
    button:"3 Brands"
  },
  {
    img:nation,
    text:"Nations Group Is An Automated Investment Syndicate Which Invests Into Brands That Develop Domestic Marketplaces With A Given Country.",
    button:"3 Brands"
  }
]
const responsive = {
  desktop: {
    breakpoint: { max: 10000, min: 1024 },
    items: 9,
    partialVisibilityGutter: 40,
    slidesToSlide: 9
  },
  tablet: {
    breakpoint: { max: 1024, min: 464 },
    items: 2,

    slidesToSlide: 2
  },
  mobile: {
    breakpoint: { max: 464, min: 0 },
    items: 2,

    slidesToSlide: 2,
  }
};
const images = [

  "https://images.unsplash.com/photo-1550133730-695473e544be?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=800&q=60",
  "https://images.unsplash.com/photo-1550167164-1b67c2be3973?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=800&q=60",
  "https://images.unsplash.com/photo-1550338861-b7cfeaf8ffd8?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=800&q=60",
  
  "https://images.unsplash.com/photo-1550353175-a3611868086b?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=800&q=60",
  "https://images.unsplash.com/photo-1550330039-a54e15ed9d33?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=800&q=60",
  "https://images.unsplash.com/photo-1549737328-8b9f3252b927?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=800&q=60",
  "https://images.unsplash.com/photo-1549833284-6a7df91c1f65?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=800&q=60",
  "https://images.unsplash.com/photo-1549985908-597a09ef0a7c?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=800&q=60",
  "https://images.unsplash.com/photo-1550064824-8f993041ffd3?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=800&q=60"
];

const sapple=[
  {
    name:"IPhone X",
    color:"#E62129"
  },
 
  {
    name:"IPhone X",
    color:"#FFA001"
  },
  {
    name:"IPhone X",
    color:"#FFDA2D"
  },
  {
    name:"IPhone X",
    color:"#E62129"
  },
  {
    name:"IPhone X",
    color:"#E62129"
  },
]

const dropdownnname=[
    {
        name:"Indicies",
    },
    {
        name:"Companies",
    },{
        name:"Platforms",
    },{
        name:"Protocols",
    },
    {
        name:"Our Story",
    },
    {
        name:"Global Footprint",
    }
]

const modeldata=[
    {
      img:venu,
      name:"VentureIndex"
    },
    {
      img:comp,
      name:"Companies"
    },{
      img:setting,
      name:"Entrepreneur",
    },
    {
      img:inventry,
      name:"Investors"
    },
  ]
export default function NavbarHome() {
  const history = useHistory();
    let listener = null
    const [scrollState, setScrollState] = useState("sub")
    const [navimage,setnavimage]=useState(bcomp)
   const [navname,setnavname]=useState('Companies')
   const  [param,setparam]=useState('Find A Companies....')
   const [textvalue,settextvalue]=useState('')
const [show,setshow]=useState(false)
const [listserch,setlistserch]=useState(sapple)
const [showsearch,setshowsearch]=useState(false)
const agency = useContext(Agency)
const {compainesvalue ,fethbrandimgae,handleatlasChangefliterAtlas,valuecompaines,imageloading} = agency;
const namechange=(e)=>{
    let s=e.target.value
    settextvalue(s)
if(e.target.value.length>0)
{
if(navname=="Copouns"||navname=="Brands"||navname=="Categories")
{
   // setshowsearch(true)
   
    // let n1= sapple.filter((user)=>{
    //     return (user.name).toUpperCase().includes((s).toUpperCase());
        
    //    })
    // setlistserch(n1)
}
}
else{
   // setshowsearch(false)
}
}
   const nvabaritem=(e)=>{
  

    settextvalue('')
    setshow(false)
    if(e.name=="VentureIndex")
    {
        setparam("Find An Venture Index....") 
        setnavname(e.name)
        setnavimage(venusb)
        history.push('/Indices')
    }

    else if(e.name=="Companies")
    {
        setparam("Find A Companies....") 
        setnavname(e.name)
        setnavimage(bcomp)
      }
    
    else if(e.name=="Promotions")
    {
        setparam("Enter Promo Code....") 
    
        setnavname("Promo") 
       }
    
    else if(e.name=="Categories")
    {
        setparam("Search Product Categories...") 
        setnavname(e.name)
      }
    else{
      setshow(true)
    }

   }
   const [prev,setprev]=useState(0)
   const [next,setnext]=useState(9)
const [elemetitem,setelemetitem]=useState([])
const [idicate, setidicate]=useState(true)
const [companiesbol,setcompaniesbol]=useState(true)
const [belemetitem,setbelemetitem]=useState([])
const [imagetoggle,setimagetoggle]=useState(true)
const [sectionimagetoggle,setsectionimagetoggle]=useState(true)
   const nextFucntion=(e)=>{

    if(next<18){
    setprev(next)
    setnext(next + e)
    }   
   }

   const prevFucntion=(e)=>{
if(prev>0){
    setprev(prev - e)
    setnext(next -e)
}

   }

 
  const onloadingidicate1=async()=>{

    await setsectionimagetoggle(false)
   }

  
  const fethimgae=async()=>{

    let data = await axios.get(
      "https://comms.globalxchange.io/gxb/app/gxlive/operator/app/groups/get"
    );
    if (data.data.status) {
      console.log("data.data.apps", data.data.apps);
    setelemetitem(data.data.groups)
    setidicate(false)
      // setbackgroudimage(data.data.apps[0].cover_photo);
      // setappicon(data.data.apps[0].app_icon);
    }
  
  }
  const onloadingimage=async()=>{
  
    await setimagetoggle(false)

   
  }


    useEffect(() => {

      fethimgae()
 

      
        
        return () => {
        }
    }, [])
  
    const myRef = useRef(null)
    const executeScroll = () => scrollToRef(myRef)

    const scrollToView=()=>{
      window.scrollTo({
        top: 500,
        behavior: "smooth"
   });
     
    }

    const  pushfunct=(e)=>{
      if(e=="Global Footprint"){
        history.push('/Globalfootprint')
      }
    }
    useEffect(() => {
     
      listener = document.addEventListener("scroll", e => {
        var scrolled = document.scrollingElement.scrollTop
        if (scrolled >= 100) {
       
            setScrollState("scrollsub")
        } else {
          if (scrollState !== "top") {
            setScrollState("sub")
          }
        }
      })
      return () => {
        document.removeEventListener("scroll", listener)
  
      }
    }, [scrollState])

const custonfunction=()=>{
  fethbrandimgae()
  history.push('/')
}

    return (
        <>
<div className="m-navbar">
<div className="sub">
  <img className="img" src={burger} alt="" />
  <img src={mlogo} alt="" />
</div>
<div className="m-top">
    {
        dropdownnname.map(item=>{
            return(
<HashLink   to={`/#${item.name}`} scroll={(el) => el.scrollIntoView({ behavior: 'auto', block: 'end' })}><p style={{fontSize:"12px"}}>{item.name}</p></HashLink >
            
            
               
            )
        })
    }
    </div>
</div>


    <div>

  
<div className="header-wrapper"  onClick={()=>  setshowsearch(false)}>
<div className="super-header ">
    <div className="content-wrapper ">
<div className="navbar-header ">
    <div className="brand" onClick={custonfunction}>
<div className={scrollState}>
    {
        scrollState=="sub"?
        <img src={logo} draggable="false" alt=""/>
        :
        <img src={scrolllogo} draggable="false" alt=""/>
        
    }

</div>
    </div>
    <div className="top">
    {
        dropdownnname.map(item=>{
            return(
<Link  onClick={scrollToView} to={item.name}        delay={0}
     duration={100}       offset={-125}

 spy={false}  smooth={true}   ><p onClick={()=>pushfunct(item.name)}>{item.name}</p></Link >
             
            
               
            )
        })
    }
    </div>

</div>
    </div>

</div>
<div className="sub-header">
    <div className="d-flex " style={{height:"100%",background:"white"}}>
    <div className="inputsection">
    <input type="text" placeholder={param} onChange={handleatlasChangefliterAtlas}/>
    <img src={searchh} alt="" />
    </div>
<div className="logosection" onClick={()=>setshow(true)}>
    <img src={navimage} alt=""/>
  
</div>
</div>
</div>
</div>
</div> 




<M
modeldata={modeldata}
navname={navname}
nvabaritem={nvabaritem}
show={show}
listserch={listserch}
showsearch={showsearch}
setshowsearch={setshowsearch}
setshow={setshow}

/>


        </>
    )
}
