import React,{useEffect,useContext,useState} from 'react'
import Carousel from "react-multi-carousel";
import './HomePage.scss'
import "react-multi-carousel/lib/styles.css";
import {Homes} from './HomeApi'
import cashback from '../../static/images/cashback.png'
import TopStories from './TopStories'
import CashBack from './CashBack'
import Poular from './Popular'
import PopularCatories from './PopularCatories'
import Loan from './Loansection'
import { Modal } from 'react-bootstrap'
import india from '../../static/images/india.png'
import apple from '../../static/images/apple.png'
import applelogo from '../../static/images/applelogo.png'
import today from '../../static/images/today.png'
import { useHistory, useParams } from "react-router-dom";
import middlecoupon from '../../static/images/middlecoupon.png'
import bb from '../../static/images/bb.png'
import up from '../../static/images/up.png'
import roicoin from '../../static/images/roicoin.png'
import watch from '../../static/images/watch.png'
import ventures from '../../static/images/ventures.png'
import { Agency } from '../../context/Context';
import enteripreess from '../../static/images/enteripreess.png'
import gril from '../../static/images/gril.png'
import lock from '../../static/images/lock.png'
import inventory from '../../static/images/inventory.png'
import startup from '../../static/images/startup.png'
import lauch from '../../static/images/lauch.png'
import back2 from '../../static/images/back2.png'
import back1 from '../../static/images/back1.png'
const itemmain=[
  {
    button1:watch,
    button2:lauch,
    logo:startup,
    back:back1
  },
  {
    button1:watch,
    button2:lauch,
    logo:ventures,
    back:back2
  }
]

const responsive = {
  desktop: {
    breakpoint: { max: 10000, min: 1024 },
    items: 2,
    partialVisibilityGutter: 40,
    slidesToSlide: 2
  },
  tablet: {
    breakpoint: { max: 1024, min: 464 },
    items: 2,

    slidesToSlide: 2
  },
  mobile: {
    breakpoint: { max: 464, min: 0 },
    items: 2,

    slidesToSlide: 2,
  }
};
const images = [
  startup,
  ventures,
  "https://d3pzq99hz695o4.cloudfront.net/sitespecific/in/banner/web/be77deea2adf00e2beec14a316bed6dc/banner_home-290x580.jpg?526085",
  "https://d3pzq99hz695o4.cloudfront.net/sitespecific/in/banner/web/066e4334c4f3e7879a6606040b498282/banner_home-290x580.jpg?609092",
  "https://d3pzq99hz695o4.cloudfront.net/sitespecific/in/banner/web/3d8fcab9d683b8c86f3cb3fa3ab89563/banner_home-290x580.jpg?672518",
  "https://d3pzq99hz695o4.cloudfront.net/sitespecific/in/banner/web/c96091c368bcb58249891929c6222e24/banner_home-290x580.jpg?467538",
  "https://images.unsplash.com/photo-1550353175-a3611868086b?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=800&q=60",

 "https://d3pzq99hz695o4.cloudfront.net/sitespecific/in/banner/web/d368acbff0cfca8c7521d9a8575d6229/banner_home-290x580.jpg?331310",
  "https://images.unsplash.com/photo-1550330039-a54e15ed9d33?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=800&q=60",
  "https://images.unsplash.com/photo-1549737328-8b9f3252b927?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=800&q=60",
  "https://images.unsplash.com/photo-1549833284-6a7df91c1f65?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=800&q=60",
  "https://images.unsplash.com/photo-1549985908-597a09ef0a7c?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=800&q=60",
  "https://images.unsplash.com/photo-1550064824-8f993041ffd3?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=800&q=60"
];

export default function MainCarsole({modeldata,footerele,setshow,setshowsearch,show,nvabaritem,navname,listserch,showsearch}) {
  const history = useHistory();
  const agency = useContext(Agency)
  const {banner } = agency;
    useEffect(() => {
        let multiples=[];
        let finial=[]
        for (var i = 0; i <= Homes.length; i++) {
            if (i % 9 === 0) { // divide by the number
              multiples.push(i); // add the current multiple found to the multiples array
            }
            else{
               
            }
          }
          for (var i = 1; i < multiples.length; i++) {
            if (i % 1 === 0) { // divide by the number
                finial.push(i); // add the current multiple found to the multiples array
            }
            else{
               
            }
          }
        console.log("dasd", finial)
        console.log("dasd", multiples.length)
        
        return () => {
        }
    }, [])
    return (
        <>

{/* <div className="topSection ">
<div className="sub">
  <h2>The Future Of Venture Capital</h2>
    <p>Tokenized Index Funds For Early Stage Investing</p>
    <div className="label">
    <label className="btnTalk" htmlFor="" onClick={()=> window.open("https://venturesindex.com/", '_blank')} ><img src={inventory} alt="" /></label> 
    <label className="btnTalk1" htmlFor="" onClick={()=> window.open("https://entrepreneurs.app/", '_blank')}><img src={enteripreess} alt="" /></label> 
    </div>
</div>
<div className="girlsection">
  <img src={gril} alt="" />
</div>
</div>



        <div className="carsoulesection ">
      
        <div className="sub">
  <h2>The Future Of Venture Capital</h2>
    <p>Tokenized Index Funds For Early Stage Investing</p>
    <div className="label">
    <label className="btnTalk" htmlFor="" onClick={()=> window.open("https://venturesindex.com/", '_blank')} ><img src={inventory} alt="" /></label> 
    <label className="btnTalk1" htmlFor="" onClick={()=> window.open("https://entrepreneurs.app/", '_blank')}><img src={enteripreess} alt="" /></label> 
    </div>
</div>



        </div>

        <div className="roicoinmain-mobile">

       
<div className="roicoin">
<img className="img"src={roicoin} alt=""/>
  <div className="pricesection">
  <h4>$3.21</h4>
<p>(1.23%)</p>
<img src={up} alt="" />
  </div>



</div>
</div>

{
  footerele?
  <div className="roicoinmain">

       
  <div className="roicoin">
  <img src={roicoin} alt=""/>
    <div className="pricesection">
    <h4>$3.21</h4>
<p>(1.23%)</p>
<img src={up} alt="" />
    </div>



<h4>$342,335.36</h4>
<h4>1,000,000 ROI</h4>
<label htmlFor=""onClick={()=> window.open("https://roicoin.com/", '_blank')}>Learn More</label>
</div>
</div>
  :

  ""
} */}


        {/* <TopStories/> */}
        {/* <div className="bannersection" style={{marginTop:"2rem"}}>
    <img src="https://d3pzq99hz695o4.cloudfront.net/temporary/image/20210414042112-73306-original.webp" alt=""/>

</div>
        <CashBack/>

        <Poular/>

        <PopularCatories/>
        <Loan/> */}
<div className=""> 


        <Modal show={show} dialogClassName="my-custommdoelterminal" onHide={()=>setshow(false)}  backdropClassName="tersetting"  aria-labelledby="contained-modal-title-vcenter" centered>
           
           <div className="userdeleteheaderterimal">
               Select The Search Category
           </div>
 
   <Modal.Body>
       <div className="coponmodel">
          {
            modeldata.map(item=>{
              return(
                <div className="sub"  onClick={()=>nvabaritem(item)}>
                  <div className="imgsection">
                    <img src={item.img} alt=""/>
                  </div>
                  <p style={item.name==navname?{fontWeight: "bold"}:{}}>{item.name}</p>
                </div>
              )
            })
          }
      
       </div>

   </Modal.Body>

</Modal>
</div>
{
  showsearch?

 <div className="searchsection">
<div className="maincontent"> 
  <div className="first">
  <div className="firstsub" style={{backgroundImage: `url(${today})`}}>
    
    </div>  
    <div className="firstsub" style={{backgroundImage: `url(${bb})`}}>
    
    </div>
  </div>
  <div className="searchlist">
    {
      listserch.map(item=>{
        return(
          <div className="inside"  style={{borderRight:`20px solid  ${item.color}`}} onClick={()=>setshowsearch(false)}>
            <div className="back" style={{backgroundImage: `url(${apple})`}}>

            </div>
            <div className="sub">
              <div className="secondsub">

              
            <p>{item.name}</p>
            <div className="title">
              <img src={applelogo} alt=""/>
              <h5>Apple</h5>

            
              </div>
              <div className="labelsection">
  <label htmlFor="">
    <img src={india} alt=""/>
  </label>
  <label htmlFor="">
    <img src={india} alt=""/>
  </label>
</div>
              </div>
              <div className="thridse">
              <h3>$780.35</h3>
                <h2>25% OFF</h2>
              </div>
            </div>
           
          </div>
        )
      })
    }
  </div>
</div>
</div> 

:""}

{/* 
{
  banner?
<div className="binnercutom">

<img src={plues} alt=""/>
<img src={middlecoupon} alt=""/>
<img src={lock} alt=""/>

</div>

  :

<div className="binner">
<div className="leftside">
  <div>
  Get Started
  </div>
  <div onClick={()=>history.push('/login')}>
Login
  </div>
</div>

<div className="righside">
  <div>
    <img src={ios} alt=""/>
  </div>
  <div>
    <img src={andr} alt=""/>
  </div>
</div>
</div>


} */}

</>
    )
}
