import React,{useContext} from 'react'
import './Loagoin.scss'


import TerminalLandingLogo from '../../static/images/logoin.png'
import retailadmin from '../../static/images/asdas.png'
import { Agency } from '../../context/Context';
import Full from './FullpageAnimation'
// import landingback from '../../../assets/backbuttons.png'
import axios from 'axios'
import { useHistory, useParams } from "react-router-dom";
export default function TerminaLogoinPage({}) {
    const agency = useContext(Agency)
    const {functionbanner } = agency;
    const history = useHistory();
    const [eplace,seteplace]=React.useState(true)
    const [pplace,setpplace]=React.useState(true)
    const [Loading,setLoading]=React.useState(false)
  const [email,setemail]=React.useState('')

  const [password,setpassword]=React.useState('')
  const emailfunc=(e)=>{
    setemail(e.target.value)
if(e.target.value.length>=1){
    seteplace(false) 
    
}
else{
    seteplace(true)   
}
  }
  const passwordfunc=(e)=>{
    setpassword(e.target.value)
    if(e.target.value.length>=1){
        setpplace(false) 
    }
    else{
        seteplace(true)   
    }
  }
    const handlefinalclick = () => {
        setLoading(true);
        axios.post(`https://gxauth.apimachine.com/gx/user/login`, {
            email: email, password: password
        }).then((res) => {
          if (res.data.status) {
            if (res.data.status === true) {
              if (res.data.mfa === true) {
                alert(
                  "It Seems That You Need To Verify Your 2FA. Please Do That Now"
                );
                
                // setError(null);
                setLoading(false);
                return;
              }
            }
            document.documentElement.setAttribute("data-theme", "light");
            localStorage.setItem("userEmail",email);
            localStorage.setItem("userEmailPermanent",email);
            localStorage.setItem("deviceKey", res.data.device_key);
            localStorage.setItem("accessToken", res.data.accessToken);
            localStorage.setItem("idToken", res.data.idToken);
            localStorage.setItem("refreshToken", res.data.refreshToken);
            functionbanner()
          history.replace("/")
           
    
            setLoading(false);
          } else {
          alert("Invalid email and password")
          setLoading(false);
          }
          if (res.data.status === false) {
            if (res.data.resend_code === true) {
              alert(
                "It Seems That You Have Not Finished Your Email Registration From When You First Signed Up. Please Confirm Your Email To Continue Logging In"
              );
             
            
              setLoading(false);
            }
          }
          if (res.data.status === false) {
            if (res.data.mfa === true) {
              alert(
                "It Seems That You Need To Verify Your 2FA. Please Do That Now"
              );
              
             
              setLoading(false);
            }
          }
    
          if (res.data.status === false) {
            if (res.data.resetPassword === true) {
              alert(
                "It Seems That You Need To Reset Your Password. Please Do That Now"
              );
        
              setLoading(false);
            }
          }
        });
      };
    
    return (

        <>
{
    Loading
    ?
    <Full/>
    :        <div className="TerinmalLandingLogoin">
    <div className="overlap">

    </div>

    <img className="admin"  src={retailadmin} alt=""/>
    
    <div className="logoinSection">
      
        <img src={TerminalLandingLogo} alt=""/>
        <div className="inside">
        {
            eplace?
            <p>Email</p>
            :""
        }
        <input type="text"  value ={email} onChange={emailfunc} />
        </div>
        <div className="inside">
        {
            pplace?
            <p>Password</p>
            :""
        }
        
        <input type="password" value ={password}   onChange={passwordfunc}/>
       
        </div> 
        <label onClick={handlefinalclick} >Login</label>
     
        
        </div>
 
</div>
}


        </>

    )
}
