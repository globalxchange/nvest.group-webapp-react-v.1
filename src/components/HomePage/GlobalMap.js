import React, { memo ,useContext,useState} from "react";
import { Agency } from '../../context/Context';
import {
  ZoomableGroup,
  ComposableMap,
  Geographies,
  Geography
} from "react-simple-maps";

const geoUrl =
  "https://raw.githubusercontent.com/zcreativelabs/react-simple-maps/master/topojson-maps/world-110m.json";

const rounded = num => {
  if (num > 1000000000) {
    return Math.round(num / 100000000) / 10 + "Bn";
  } else if (num > 1000000) {
    return Math.round(num / 100000) / 10 + "M";
  } else {
    return Math.round(num / 100) / 10 + "K";
  }
};

const MapChart = ({ setTooltipContent }) => {
    const [clickedCity, setClickedCity] = useState("");
    const agency = useContext(Agency)
    const {compainesvalue,flitermapfunction } = agency;
    const handleClick = (geo) => {
      setClickedCity(geo.properties.name);
      
    
    };
  return (
    <>
      <ComposableMap data-tip="" >
        <ZoomableGroup>
          <Geographies geography={geoUrl}>
            {({ geographies,projection }) =>
              geographies.map(geo => {
                const isClicked = clickedCity === geo.properties.name;
                return(
                <Geography
                  key={geo.rsmKey}
                  geography={geo}
                  projection={ projection }
                  onMouseEnter={() => {
                    const { NAME, POP_EST } = geo.properties;
                    setTooltipContent(`${NAME}`);
                  }}

                  onClick={() => {
                    const { NAME, POP_EST } = geo.properties;
                    flitermapfunction(`${NAME}`);
                  }}
                
                    
                    
                  onMouseLeave={() => {
                    setTooltipContent("");
                    
                  }}
                  width={800}
                  height={400}
                  style={{
                   
                    default: {
                      fill: "#B8B9BA",
                      outline: "none"
                    },
                    hover: {
                        fill: "#182542",
                        outline: "none"
                      },
                      pressed: {
                        fill: "#182542",
                        stroke: "#607D8B",
                        strokeWidth: 1,
                        outline: "none",
                     }
                  }}
                />
              )
                }
              )
            }
          </Geographies>
        </ZoomableGroup>
      </ComposableMap>
    </>
  );
};

export default memo(MapChart);
