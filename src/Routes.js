import React,{useEffect} from "react";
import { Switch, Route, BrowserRouter } from "react-router-dom";
import ConverterPage from "./pages/ConverterPage";
import HomePage from "./components/HomePage/NavbarHome";
import Login from "./components/HomePage/LogoinPage";
import Gloabl from "./components/HomePage/ToolMap"
import Companiess from "./components/HomePage/CompaniesMain"
import Indices from "./components/HomePage/IdicateMainComponent"


function Routes() {

  useEffect(() => {
    window.scrollTo(0, 0);
    
    return () => {
      
    }
  }, [])
  return (
    <React.Fragment>
      <Switch>
        <Route exact path="/" component={HomePage} />
        <Route exact path="/login" component={Login} />
        <Route exact path="/Globalfootprint" component={Gloabl} />
        <Route exact path="/Companies" component={Companiess} />
        <Route exact path="/Indices" component={Indices} />
      </Switch>
      </React.Fragment>
  );
}

export default Routes;
